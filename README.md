# Introduction
Enigma is a one of three PCB boards composing the telemetry stack designed for IONE 2.0, an innovative H2 based catamaran.
Essentially, this is a revisited STM32 Nucleo Board, with additional features. It's based on the STM32F446 MCU by STMicroelectronics and it's fully personalized for the IONE 2.0 project. 

> For this reason, the pin mapping is completely different from a usual STM Nucleo!

# Features

Here a list of the additional features present on this board

- The pin connectors are **mechanically** compatible with the Nucleo one (but the pin mapping is different!). In addition, on this board are present some other connectors present in a standard STM32 Nucleo, like the one for programming, the current measurement, the external uart...
- **CAN transceiver** on board, with a dedicated connector and the 120R terminal resistor.
- **Buck converter**, with a 1,5A max total output current on connector pins. This buck converter has a high efficency at very low current, 
allowing an efficent usage of the board at low current.
- **Current monitoring** circuit: essentially, it's a circuit that using a shunt resistor placed at the very input in allows the reading of the total current consumption of the entire board, converting it in a voltage directly connected to a specific MCU pin.
- **Current protection** circuit: this circuit monitors the "current control circuit" output in order to limit the current consumption. The current limit is settable changing an SMD resistor (not a variable resistor).
- **ON/OFF switch**: there is the possibility to solder a button in order to control the ignition of the board. This button operates directly on the board power supply, this means that when is open no pheriperal is powered.
- **Low pass filer** on each analog input of the MCU.
- External connector for a GPIO
- External connector for a UART
